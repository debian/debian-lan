#!/bin/bash

set -e

fcopy -m root,root,0600 /etc/sssd/sssd.conf

## faiconfig:
fcopy -ir /etc/fai

## authorized_keys for root:
fcopy -ir /root

## Fetch proxy information from wpad.dat:
WPAD=/tmp/wpad.dat
export http_proxy="";
if which curl ; then
    curl --fail --silent http://wpad.intern/wpad.dat > $WPAD
elif which wget ; then
    wget --output-document=$WPAD http://wpad.intern/wpad.dat
fi

PROXY=$(grep PROXY "$WPAD" | sed "s/^.*PROXY //" | sed "s/;.*$//")
ainsl /etc/environment "http_proxy=http://$PROXY/"

if [ "$PROXY" == "webcache:8080" ] ; then
    ## dansguardian is used, lock firefox proxy configuration:
    ainsl /etc/firefox-esr/firefox-esr.js  'lockPref("network.proxy.type", 4);'
else
    ## default configuration to wpad.dat proxy:
    ainsl /etc/firefox-esr/firefox-esr.js  'pref("network.proxy.type", 4);'
fi

## Allow SSO in firefox:
ainsl /etc/firefox-esr/firefox-esr.js 'pref("network.negotiate-auth.delegation-uris", "intern");'
ainsl /etc/firefox-esr/firefox-esr.js 'pref("network.negotiate-auth.trusted-uris", "intern");'

## Default Homepage:
ainsl /etc/firefox-esr/firefox-esr.js 'pref("browser.startup.homepage", "data:text/plain,browser.startup.homepage=http://www.intern");'

## Optional: Switch off caching over the network (eventually in combination with unburden-home-dir):
#ainsl /etc/firefox-esr/firefox-esr.js  'pref("browser.cache.disk.enable", false);'
#ainsl /etc/firefox-esr/firefox-esr.js  'pref("browser.cache.offline.enable", false);'
#ainsl /etc/firefox-esr/firefox-esr.js  'pref("browser.safebrowsing.enabled", false);'
#ainsl /etc/firefox-esr/firefox-esr.js  'pref("browser.safebrowsing.malware.enabled", false);'
