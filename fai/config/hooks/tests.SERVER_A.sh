#!/bin/bash
#
#

#
# Enable services again when converting a minimal installation.
# Only do that when converting:
if [ -n "$POLICYFILE" ] ; then
    rm $POLICYFILE

    ## unmask the units again:
    systemctl unmask slapd
    systemctl unmask isc-dhcp-server
fi

#
# Run etckeeper:
#

$ROOTCMD etckeeper commit "saving modifications after configure scripts run" || true
