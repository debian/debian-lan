#! /bin/bash
#
#  Disable services when converting a minimal installation.
#  Create necessary directories if missing.


POLICYFILE="/usr/sbin/policy-rc.d"

## Only when converting:
if [ "$CONVERT" == "true" ] && [ "$target" == "/" ] && [ ! -e $POLICYFILE ] ; then
    cat > $POLICYFILE <<EOF
#!/bin/sh
exit 101
EOF
    chmod a+rx $POLICYFILE
    mkdir -p /var/lib/fai/config
    mkdir -p /lan/mainserver/home0

    ## for systemd, mask the units:
    ln -fs /dev/null /etc/systemd/system/multi-user.target.wants/slapd.serivce
    ln -fs /dev/null /etc/systemd/system/multi-user.target.wants/isc-dhcp-server.service
else
    unset POLICYFILE
fi
