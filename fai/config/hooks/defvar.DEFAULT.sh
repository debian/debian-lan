#! /bin/bash
#
#   Detect all available NICs
#

## Variable containing available network interfaces:

N=0
fields="ID_NET_NAME_FROM_DATABASE ID_NET_NAME_ONBOARD ID_NET_NAME_SLOT ID_NET_NAME_PATH"
for NIC in $(ip link show | grep -E "^\w+:" | cut -d ":" -f2 | grep -v lo) ; do
    for field in $fields; do
        name=$(udevadm info /sys/class/net/$NIC | sed -rn "s/^E: $field=(.+)/\1/p")
        if [[ $name ]]; then
            echo "NIC_LABEL${N}=$name" >> $LOGDIR/additional.var
            N=$((N+1))
            break
        fi
    done
done
